@extends('layouts/app')
@section('title', $title)

@section('container')
<div class="container-fluid"> 
	<div class="row">
		<div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="m-0">{{ $title }}</h4>
        </div>
        <div class="card-body">
          <form action="{{ $action }}" method="POST">
            @csrf
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <label for="kategori-title">Judul Kategori</label>
                  <input type="text" class="form-control" id="kategori-title" placeholder="" name="title" value="{{ $kategori->judul ?? '' }}" required>
                </div>
              </div>
              <div class="col-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
		</div>
	</div>
</div>
@endsection