@extends('layouts/app')
@section('title', 'Tabel Kategori')

@section('container')
<div class="container-fluid"> 
	<div class="row">
		<div class="col-12">
      <div class="card">
        <div class="card-header">
          <div class="d-flex justify-content-between align-items-center">
            <div>
              <h4 class="mt-1 mb-0">Tabel Kategori</h4>
            </div>
            <div class="text-right">
              <a href="{{ url('/category-new') }}" class="btn btn-primary">Tambah</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <table class="table bg-white">
            <thead class="bg-dark text-white thead-dark">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Judul</th>
                <th scope="col">Daftar Produk</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
            @foreach($kategoris as $index => $item)
              <tr>
                <th scope="row">{{($index + 1)}}</th>
                <td>{{$item->judul}}</td>
                <td><a href="{{url("list-product/$item->id")}}">Tabel Produk {{$item->judul}}</a></td>
                <td class="text-right pr-3">
                  <a href="{{ url("/category-edit/$item->id") }}" class="text-primary">
                    <i width="18" height="18" data-feather="edit"></i>
                  </a>
                  <a href="{{ url("/category-delete/$item->id") }}" class="text-danger" onclick="return confirm('Yakin akan menghapus Kategori {{ $item->judul }}?')">
                    <i width="18" height="18" data-feather="trash"></i>
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
