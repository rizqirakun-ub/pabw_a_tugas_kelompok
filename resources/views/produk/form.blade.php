@extends('layouts/app')
@section('title', 'Daftar Barang')

@section('container')
<div class="container-fluid"> 
  <div class="row">
		<div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="m-0">{{ $title }}</h4>
        </div>
        <div class="card-body">
          <form action="{{ $action }}" method="POST">
            @csrf
            <div class="row">
              <div class="col-4">
                <div class="form-group">
                  <label for="product-category">Kategori</label>
                  <select class="form-control" id="product-category" name="category">
                      @foreach ($kategoris as $item)
                          <option {{ ($item->id == ($product->kategori_id ?? null) ? 'selected' : '') }} value="{{ $item->id }}">{{ $item->judul }}</option>
                      @endforeach
                  </select>
                </div>
              </div>

              <div class="col-8">
                <div class="form-group">
                  <label for="product-name">Nama Produk</label>
                  <input type="text" class="form-control" id="product-name" placeholder="" name="name" value="{{ $product->namaProduk ?? '' }}" required>
                </div>
              </div>

              <div class="col-12">
                <div class="form-group">
                  <label for="product-description">Deskripsi</label>
                  <textarea class="form-control" rows="3" name="description" id="product-description" cols="30" rows="10" required>{{ $product->deskripsiProduk ?? '' }}</textarea>
                </div>
              </div>

              <div class="col-4">
                <div class="form-group">
                  <label for="product-code">Kode SKU</label>
                  <input type="text" class="form-control" id="product-code" placeholder="" name="code" value="{{ $product->kodeProduk ?? '' }}" required>
                  {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                </div>
              </div>

              <div class="col-4">
                <div class="form-group">
                  <label for="price">Harga</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Rp</span>
                    </div>
                    <input class="form-control" id="price" type="number" name="price" placeholder="" value="{{ $product->price ?? '' }}" required>
                  </div>
                </div>
              </div>

              <div class="col-4">
                <div class="form-group">
                  <label for="weight">Berat</label>
                  <div class="input-group">
                    <input class="form-control" id="weight" type="number" name="weight" placeholder="" value="{{ $product->weight ?? '' }}" required>
                    <div class="input-group-append">
                      <span class="input-group-text">gram</span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-12">
                <div class="form-group">
                  <label for="image">Link Gambar</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i width="18" height="18" data-feather="link"></i>
                      </span>
                    </div>
                    <input class="form-control" id="image" name="image" type="text" placeholder="https://" value="{{ $product->image ?? '' }}">
                  </div>
                </div>
              </div>

              <div class="col-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
		</div>
	</div>
</div>
@endsection