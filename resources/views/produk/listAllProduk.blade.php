@extends('layouts/app')
@section('title', $title ?? '')

@section('container')
<div class="container-fluid"> 
	<div class="row">
		<div class="col-12">
      <div class="card">
        <div class="card-header">
          <div class="d-flex justify-content-between align-items-center">
            <div>
              <h4 class="mt-1 mb-0">{{ $title ?? '' }}</h4>
            </div>
            <div class="text-right">
              <a href="{{ url('/product-new') }}" class="btn btn-primary">Tambah</a>
            </div>
          </div>
        </div>
        <div class="card-body">
          @if (count($products) > 1)
          <table class="table bg-white">
            <thead class="bg-dark text-white thead-dark">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Gambar</th>
                <th scope="col">Kode</th>
                <th scope="col">Nama</th>
                <th scope="col">Harga</th>
                {{-- <th scope="col">Deskripsi</th> --}}
                <th scope="col">Berat</th>
                <th scope="col">Kategori</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
            @foreach($products as $index => $item)
              <tr>
                <td scope="row">{{($index + 1)}}</td>
                <td>
                  <div class="card overflow-hidden m-0" style="width: 40px; height: 40px;">
                    @if (!empty($item->image))
                    <img src="{{ $item->image }}" alt="{{$item->namaProduk}}" class="img-fluid">
                    @else
                    <div class="d-flex align-items-center justify-content-center w-100 h-100">
                      <i data-feather="image" stroke-width="1" class="text-muted"></i>
                    </div>
                    @endif
                  </div>
                </td>
                <td>{{$item->kodeProduk}}</td>
                <td>{{$item->namaProduk}}</td>
                <td>Rp {{ number_format($item->price, 0, ',', '.') }}</td>
                {{-- <td>{{$item->deskripsiProduk}}</td> --}}
                <td>{{$item->weight}} gram</td>
                <td>{{$item->kategori->judul}}</td>
                <td class="text-right pr-3">
                  <a href="{{ url("/product-edit/$item->id") }}" class="text-primary">
                    <i width="18" height="18" data-feather="edit"></i>
                  </a>
                  <a href="{{ url("/product-delete/$item->id") }}" class="text-danger" onclick="return confirm('Yakin akan menghapus produk {{ $item->namaProduk }}?')">
                    <i width="18" height="18" data-feather="trash"></i>
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <span>Produk Kosong</span>
          @endif
        </div>
      </div>
			  
		</div>
	</div>
</div>
@endsection