@extends('layouts/app')
@section('title', $title ?? '')

@section('container')
<div class="container-fluid"> 
	<div class="row">
		<div class="col-12">
            <div class="card">
                <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mt-1 mb-0">{{ $title ?? '' }}</h4>
                    </div>
                </div>
                </div>
                <div class="card-body">
                @if (count($transactions) > 0)
                    @foreach ($transactions as $index => $item)
                    <div class="card">
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-12 py-3 d-flex">
                                    <div class="text-muted mr-auto">
                                        <i data-feather="calendar" stroke-width="1" width="16" height="16" class="mb-1"></i>
                                        <span>{{ $item->transaction_date }}</span>
                                    </div>
                                    <div class="">
                                        <span class="badge p-2 text-uppercase {{ ($item->status == 'unpaid') ? 'badge-danger' : 'badge-success' }}">{{ $item->status }}</span>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div>
                                        <strong>{{ $item->customer }}</strong>
                                        <p>
                                            {{ $item->address }}
                                        </p>
                                    </div>

                                    @foreach ($item->detail as $detail)
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="card overflow-hidden m-0 mr-3" style="width: 40px; height: 40px;">
                                            @if (!empty($detail->product->image))
                                            <img src="{{ $detail->product->image }}" alt="{{ $detail->product->namaProduk }}" class="img-fluid">
                                            @else
                                            <div class="d-flex align-items-center justify-content-center w-100 h-100">
                                                <i data-feather="image" stroke-width="1" class="text-muted"></i>
                                            </div>
                                            @endif
                                        </div>
                                        
                                        <div class="mb-2">
                                            {{ $detail->product->namaProduk }} x {{ $detail->qty }}
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="col-4">
                                    @if (!empty($item->note))
                                    <strong>Catatan</strong>                                    
                                    <p>
                                        {{ $item->note }}
                                    </p>
                                    @endif
                                </div>
                                
                                <div class="col-3" style="border-left: 1px solid #efefef">
                                    <p>
                                        <span class="text-muted">Total Belanja</span>
                                        <br>
                                        <b>Rp {{ number_format($item->payment, 0, ',', '.') }}</b>
                                    </p>
                                    <p>
                                        <span class="text-muted">Total Berat</span>
                                        <br>
                                        <b>{{ $item->total_weight }} g</b>
                                    </p>
                                </div>

                                <div class="col-12">
                                    <div class="text-right">
                                        <a href="{{ url("/transaction-edit/$item->id") }}" class="text-primary">
                                            <i width="18" height="18" data-feather="edit"></i>
                                        </a>
                                        <a href="{{ url("/transaction-delete/$item->id") }}" class="text-danger" onclick="return confirm('Yakin akan menghapus transaksi {{ $item->namaProduk }}?')">
                                            <i width="18" height="18" data-feather="trash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                <span>Transaksi Kosong</span>
                @endif
                </div>
            </div>
		</div>
	</div>
</div>

@endsection