@extends('layouts/app')
@section('title', $title)

@section('container')
<div class="container-fluid"> 
	<div class="row">
		<div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4 class="m-0">{{ $title }}</h4>
        </div>
        <div class="card-body">
          <form action="{{ $action }}" method="POST">
            @csrf
            <div class="row">
              <div class="col-3">
                <div class="form-group">
                  <label for="kategori-title">Customer</label>
                  <input class="form-control" type="text" name="customer" value="{{ $transaction->customer ?? '' }}" readonly>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="kategori-title">Payment Status</label>
                  <select name="status" class="form-control">
                      <option value="unpaid">Unpaid</option>
                      <option value="paid" {{ $transaction->status == 'paid' ? 'selected' : '' }}>Paid</option>
                  </select>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="kategori-title">Total Belanja</label>
                  <input class="form-control" type="text" name="payment" value="{{ $transaction->payment ?? '' }}" readonly>
                </div>
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label for="kategori-title">Total Berat</label>
                  <input class="form-control" type="text" name="weight" value="{{ $transaction->total_weight ?? 0 }}" readonly>
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <label for="kategori-title">Address</label>
                  <textarea name="address" class="form-control" cols="30" rows="5">{{ trim($transaction->address) ?? '' }}</textarea>
                </div>
                <div class="form-group">
                  <label for="kategori-title">Note</label>
                  <textarea name="note" class="form-control" cols="30" rows="3">{{ trim($transaction->note) ?? '' }}</textarea>
                </div>
              </div>
              <div class="col-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
		</div>
	</div>
</div>
@endsection