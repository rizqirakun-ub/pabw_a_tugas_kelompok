<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://unpkg.com/feather-icons"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <style>
        .c-sidebar-nav-icon {
            font-size: 12px;
        }
    </style>
</head>
<body>
    <div class="c-app" cz-shortcut-listen="true">
        <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
            <div class="c-sidebar-brand d-lg-down-none">
                <div class="c-sidebar-brand-full">Kelompok PWL</div>
                <div class="c-sidebar-brand-minimized">PWL</div>
            </div>
            <ul class="c-sidebar-nav">
                @auth
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="/dashboard">
                        <span class="c-sidebar-nav-icon">
                            <span width="18" height="18" stroke-width="1" data-feather="airplay"></span>
                        </span>
                        Dashboard
                    </a>
                </li>
                <li class="c-sidebar-nav-title">Shopping</li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="/vue" target="_blank">
                        <span class="c-sidebar-nav-icon">
                            <span width="18" height="18" stroke-width="1" data-feather="shopping-cart"></span>
                        </span>
                        Belanja
                    </a>
                </li>
                <li class="c-sidebar-nav-title">Table List</li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="/list-all-product">
                        <span class="c-sidebar-nav-icon">
                            <span width="18" height="18" stroke-width="1" data-feather="box"></span>
                        </span>
                        Produk
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="/list-category">
                        <span class="c-sidebar-nav-icon">
                            <span width="18" height="18" stroke-width="1" data-feather="columns"></span>
                        </span>
                        Kategori
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ url('list-transaction') }}">
                        <span class="c-sidebar-nav-icon">
                            <span width="18" height="18" stroke-width="1" data-feather="shopping-bag"></span>
                        </span>
                        Transaksi
                    </a>
                </li>
                @endauth
                {{-- <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                        <span class="c-sidebar-nav-icon">
                            <span width="18" height="18" data-feather="airplay"></span>
                        </span>
                        Base
                    </a>
                    <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/breadcrumb.html"><span class="c-sidebar-nav-icon"></span> Breadcrumb</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/cards.html"><span class="c-sidebar-nav-icon"></span> Cards</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/carousel.html"><span class="c-sidebar-nav-icon"></span> Carousel</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/collapse.html"><span class="c-sidebar-nav-icon"></span> Collapse</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/forms.html"><span class="c-sidebar-nav-icon"></span> Forms</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/jumbotron.html"><span class="c-sidebar-nav-icon"></span> Jumbotron</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/list-group.html"><span class="c-sidebar-nav-icon"></span> List group</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/navs.html"><span class="c-sidebar-nav-icon"></span> Navs</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/pagination.html"><span class="c-sidebar-nav-icon"></span> Pagination</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/popovers.html"><span class="c-sidebar-nav-icon"></span> Popovers</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/progress.html"><span class="c-sidebar-nav-icon"></span> Progress</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/scrollspy.html"><span class="c-sidebar-nav-icon"></span> Scrollspy</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/switches.html"><span class="c-sidebar-nav-icon"></span> Switches</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/tables.html"><span class="c-sidebar-nav-icon"></span> Tables</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/tabs.html"><span class="c-sidebar-nav-icon"></span> Tabs</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="base/tooltips.html"><span class="c-sidebar-nav-icon"></span> Tooltips</a></li>
                    </ul>
                </li> --}}
            </ul>
            <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
        </div>

        <div class="c-wrapper c-fixed-components">
            <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
                <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto"
                    type="button" data-target="#sidebar" data-class="c-sidebar-show">
                    <svg class="c-icon c-icon-lg">
                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
                    </svg>
                </button><a class="c-header-brand d-lg-none" href="#">
                    <svg width="118" height="46" alt="CoreUI Logo">
                        <use xlink:href="assets/brand/coreui.svg#full"></use>
                    </svg></a>

                <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
                    <span width="18" height="18" stroke-width="1" data-feather="menu"></span>
                </button>

                <ul class="c-header-nav d-md-down-none">
                    <li class="c-header-nav-item px-3">
                        <a class="c-header-nav-link" href="/about">About</a>
                    </li>
                </ul>
                <ul class="c-header-nav ml-auto mr-4">
                    <li class="c-header-nav-item d-md-down-none mx-2">
                        <a class="c-header-nav-link" href="#">
                            <span width="18" height="18" stroke-width="1" data-feather="bell"></span>
                        </a>
                    </li>
                    <li class="c-header-nav-item dropdown">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{
                                __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{
                                __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle"
                                href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right"
                                aria-labelledby="navbarDropdown">
                                <a class="dropdown-item"
                                    href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form"
                                    action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </li>
                </ul>
                <div class="c-subheader px-3">
            
                    <ol class="breadcrumb border-0 m-0">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item active">@yield('title')</li>
                    </ol>
                </div>
            </header>
            <div class="c-body">
                <main class="c-main">
                    @yield('container')
                </main>
            <footer class="c-footer">
                <div>
                    <a href="https://coreui.io">CoreUI</a> © 2020 creativeLabs.
                </div>
                <div class="ml-auto">
                    <span>Powered by</span>
                    <span class="c-sidebar-nav-icon">
                        <span class="text-danger" width="18" height="18" stroke-width="1" data-feather="heart"></span>
                    </span>
                </div>
            </footer>
            </div>
        </div>

    </div>

    <script>
        feather.replace()
    </script>
    <script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
</body>
</html>
