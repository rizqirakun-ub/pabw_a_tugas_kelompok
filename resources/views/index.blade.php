@extends('layouts/app')

@section('title', 'Kelompok PWL')

@section('container')
<div class="container">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
					<span>Homepage</span>
				</div>
				<div class="card-body">
					<p>
						<h1 class="mt-3">Pemrograman Web Lanjut</h1>
						<span>Silahkan login untuk melanjutkan</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>  
@endsection