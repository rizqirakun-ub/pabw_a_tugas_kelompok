@extends('layouts/app')

@section('title', 'About')

@section('container')
<div class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="card p-3">
				<h3>Anggota Kelompok:</h3>
				<ul>
					<li>WISANG JATI ISMUWARDOYO - 185150400111026</li>
					<li>AHMAD AZMI ABDURRAHIM - 185150400111046</li>
					<li>MUHAMMAD RIZQI RAMADHAN - 185150400111054</li>
					<li>WILDAN ARRIZAL WAHYU SUTOMO - 185150407111019</li>
					<li>YUDI AGUSTRI JAYA PARDEDE - 185150407111020</li>
				</ul>
			</div>
		</div>
	</div>
</div>  
@endsection