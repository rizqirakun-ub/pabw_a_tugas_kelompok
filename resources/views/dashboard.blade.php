@extends('layouts/app')
@section('title', 'Dashboard')

@section('container')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-primary">
                    <div
                        class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{ $product_total }}</div>
                            <div>Total Produk</div>
                        </div>
                    </div>
                    <div class="c-chart-wrapper mt-3 mx-3" style="height:30px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas class="chart chartjs-render-monitor" id="card-chart1"
                            height="70"
                            style="display: block; width: 208px; height: 70px;"
                            width="208"></canvas>
                    </div>
                </div>
            </div>
        
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-info">
                    <div
                        class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{ $category_total }}</div>
                            <div>Total Kategori</div>
                        </div>
                    </div>
                    <div class="c-chart-wrapper mt-3 mx-3" style="height:30px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas class="chart chartjs-render-monitor" id="card-chart2"
                            height="70" width="208"
                            style="display: block; width: 208px; height: 70px;"></canvas>
                    </div>
                </div>
            </div>
        
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-warning">
                    <div
                        class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{ $transaction_total }}</div>
                            <div>Total Transaksi</div>
                        </div>
                    </div>
                    <div class="c-chart-wrapper mt-3" style="height:30px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas class="chart chartjs-render-monitor" id="card-chart3"
                            height="70" width="240"
                            style="display: block; width: 240px; height: 70px;"></canvas>
                    </div>
                </div>
            </div>
        
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-danger">
                    <div
                        class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{ $qty_total }}</div>
                            <div>Total Produk Terjual</div>
                        </div>
                    </div>
                    <div class="c-chart-wrapper mt-3 mx-3" style="height:30px;">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas class="chart chartjs-render-monitor" id="card-chart4"
                            height="70" width="208"
                            style="display: block; width: 208px; height: 70px;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection