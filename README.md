<h1>Tugas Kelompok PABW A</h1>

Anggota: <br>
- WISANG JATI ISMUWARDOYO - 185150400111026<br>
- AHMAD AZMI ABDURRAHIM - 185150400111046<br>
- MUHAMMAD RIZQI RAMADHAN - 185150400111054<br>
- WILDAN ARRIZAL WAHYU SUTOMO - 185150407111019<br>
- YUDI AGUSTRI JAYA PARDEDE - 185150407111020<br>

<h2>Note</h2>

0. Clone Project<br>
`git clone <alamat https git>`<br>
`composer install` => install library yang dibutuhkan laravel<br>
`copy .env.example .env` => Melakukan copy file example ke .env. Buat database file yang sudah di copy perlu disesuaikan<br> 
`php artisan key:generate` => inisiasi key projek laravel agar dapat digunakan pada local<br> 
`php artisan config:cache` => clear & save config .env<br>
`npm install` => install npm<br>

1. Start server<br>
`php artisan serve`<br>

2. Prepare Database<br>
`php artisan migrate` => buat otomatis tabel<br>
`php artisan db:seed` => isi row otomatis<br>

3. Git branching<br>
`git ls-remote origin` => lihat branch origin<br>
`git fetch origin wildan:wildan` => ambil branch wildan<br>
`git branch` => preview list wildan<br>
`git checkout wildan` => ganti branch<br>
`git branch` => preview list branch apa sudah berubah belum<br>

4. Git push & pull<br>
`git fetch` => cek update git perubahan terbaru<br>
`git pull` => ambil perubahan baru<br>
`git add .` => tambah seluruh perubahan file<br>
`git commit -m "text commit"` => commit perubahan<br>
`git push -u origin <branch>` => push perubahan ke git<br>

5. Git extras<br>
`git reset --soft HEAD^` => ga jadi commit<br>

5. NPM<br>
`npm install` => install package npm<br>
`npm run dev` => build package vue.js<br>
`npm run watch` => build package vue.js terus menerus (untuk development)<br>
