<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public function product()
    {
        return $this->hasMany('App\Produk', 'kategori_id', 'id');
    }
}
