<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $timestamps = false;

    public function product()
    {
        return $this->hasOne(Produk::class, 'id', 'produk_id');
    }
}
