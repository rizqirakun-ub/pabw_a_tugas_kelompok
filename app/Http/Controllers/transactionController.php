<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\TransactionDetail;
use App\Cart;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;

class transactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all()->sortDesc();
        $data = [
            'transactions' => $transactions,
            'title' => 'List Transaksi'
        ];
        return view('transaction.list', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $action = url("/transaction-update/$id");
        $data = ['title' => 'Edit Transaksi', 'transaction' => $transaction, 'action' => $action];
        return view('transaction.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);
        // $transaction->customer = $request->customer;
        $transaction->address = $request->address;
        $transaction->note = $request->note;
        $transaction->status = $request->status;

        $transaction->save();
        return redirect('list-transaction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);
        $transaction->detail()->delete();
        $transaction->delete();
        return redirect('list-transaction');
    }

    public function checkout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $carts = Cart::all();

        if ($carts->count() == 0) {
            return response()->json(['message' => 'keranjang kosong']);
        }

        DB::transaction(function () use ($request, $carts) {
            $totalWeight = 0;
            $totalPayment = 0;

            $transaction = new Transaction;
            $transaction->customer = $request->customer;
            $transaction->address = $request->address;
            $transaction->transaction_date = Carbon::parse($transaction['created_at'])->format('Y-m-d');
            $transaction->note = $request->note;
            $transaction->total_weight = $totalWeight;
            $transaction->payment = $totalPayment;
            
            $transaction->status = 'unpaid';
            $transaction->save();

            foreach ($carts as $index => $cart) {
                $transactionDetail = new TransactionDetail;
                $transactionDetail->transaction_id = $transaction->id;
                $transactionDetail->produk_id = $cart->produk_id;
                $transactionDetail->qty = $cart->qty;
                $transactionDetail->price = $cart->price;
                $transactionDetail->save();

                $totalWeight += $cart->qty * $transactionDetail->product->weight;
                $totalPayment += $cart->qty * $cart->price;
            }

            $transaction->total_weight = $totalWeight;
            $transaction->payment = $totalPayment;
            $transaction->save();

            Cart::truncate();
        });
        
        $data = [
            'message' => 'Berhasil',
        ];

        return response()->json($data, 200);
    }
}
