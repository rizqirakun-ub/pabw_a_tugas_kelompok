<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Produk;
use App\Kategori;

class produkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Produk::with('kategori')->get();
        $data = [
            'products' => $products,
            'title' => 'Tabel Produk'
        ];
        return view('produk.listAllProduk', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = Kategori::all();
        $data = [
            'title' => 'Tambah Produk',
            'kategoris' => $kategoris,
            'action' => url('/product-save')
        ];
        return view('produk.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = new Produk;
        $produk->kodeProduk = $request->code;
        $produk->namaProduk = $request->name;
        $produk->deskripsiProduk = $request->description;
        $produk->kategori_id = $request->category;
        $produk->weight = $request->weight;
        $produk->price = $request->price;
        $produk->image = $request->image;

        $produk->save();
        return redirect('list-all-product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoris = Kategori::all();
        $product = Produk::find($id);
        $action = url("/product-update/$id");
        return view('produk.form', ['title' => 'Edit Produk', 'product' => $product, 'kategoris' => $kategoris, 'action' => $action]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Produk::find($id);
        $produk->kodeProduk = $request->code;
        $produk->namaProduk = $request->name;
        $produk->deskripsiProduk = $request->description;
        $produk->kategori_id = $request->category;
        $produk->weight = $request->weight;
        $produk->price = $request->price;
        $produk->image = $request->image;

        $produk->save();
        return redirect('list-all-product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $produk->delete();
        return redirect('list-all-product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $produks = Produk::all();
        return $produks->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        $product = Produk::find($id);
        return response()->json(
            [
                'product' => $product,
                'kategori' => $product->kategori
            ]
        );
    }
}
