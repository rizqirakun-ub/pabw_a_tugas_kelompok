<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Cart::all();
        return $carts->toJson();
    }

    /**
     * Store and Update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upsert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'produk_id' => 'required',
            'qty' => 'required|numeric|min:1',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $id = $request->produk_id;
        $cart = Cart::where('produk_id', $id)->first();

        if ($cart === null) {
            $cart = new Cart;
            $cart->produk_id = $id;
            $cart->qty = $request->qty;
            $cart->price = $request->price;
        } else {
            $cart->qty = $cart->qty + $request->qty;
        }
        $cart->save();

        return response()->json($cart);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::destroy($id);
        return response()->json($cart);
    }

    public function counter()
    {
        $cart = Cart::all()->sum('qty');
        return response()->json($cart);
    }

    public function getAll()
    {
        $cart = Cart::with('product')->get();
        return response()->json($cart);
    }
}
