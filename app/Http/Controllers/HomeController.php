<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use App\Transaction;
use App\TransactionDetail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        $productTotal = Produk::count();
        $categoryTotal = Kategori::count();
        $transactionTotal = Transaction::count();
        $qtyTotal = TransactionDetail::get()->sum('qty');

        $data = [
            'product_total' => $productTotal,
            'category_total' => $categoryTotal,
            'transaction_total' => $transactionTotal,
            'qty_total' => $qtyTotal,
        ];
        return view('dashboard', $data);
    }
}
