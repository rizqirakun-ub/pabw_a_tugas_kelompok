<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions_detail';

    public function product()
    {
        return $this->hasOne(Produk::class, 'id', 'produk_id');
    }
}
