<?php

use Illuminate\Database\Seeder;
use App\Produk;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::insert([
            [
                'kodeProduk' => 'BJU001',
                'namaProduk' => 'Baju Merah',
                'deskripsiProduk' => 'ini adalah baju merah',
                'kategori_id' => 1,
                'weight' => 200,
                'price' => 14000,
                'image' => 'https://i.pinimg.com/564x/4a/ec/ab/4aecaba5f9ff69c67aa953d1663f819e.jpg',
            ],
            [
                'kodeProduk' => 'BJU002',
                'namaProduk' => 'Baju Biru',
                'deskripsiProduk' => 'berwarna biru langit',
                'kategori_id' => 1,
                'weight' => 230,
                'price' => 3200,
                'image' => null,
            ],
            [
                'kodeProduk' => 'CLN001',
                'namaProduk' => 'Jeans Hitam',
                'deskripsiProduk' => 'Jeans yang berwarna gelap hitam',
                'kategori_id' => 2,
                'weight' => 300,
                'price' => 0,
                'image' => null,
            ],
        ]);
    }
}
