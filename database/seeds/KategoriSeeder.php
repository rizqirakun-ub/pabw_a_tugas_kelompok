<?php

use Illuminate\Database\Seeder;
use App\Kategori;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kategori::insert([
            ['id' => 1, 'judul' => 'Baju'],
            ['id' => 2, 'judul' => 'Celana'],
        ]);
    }
}
