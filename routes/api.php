<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\kategoriController;
use App\Http\Controllers\produkController;
use App\Http\Controllers\transactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// category
Route::get('kategoris', [kategoriController::class, 'get_kategoris']);

// product
Route::get('/product/getAll', [produkController::class, 'getAll']);
Route::get('/product/get/{id}', [produkController::class, 'get']);

// cart
Route::get('/cart/all', 'CartController@getAll');
Route::get('/cart/counter', 'CartController@counter');
Route::post('/cart/upsert', 'CartController@upsert');
Route::delete('/cart/delete/{id}', 'CartController@destroy');

// transaction
Route::post('/cart/checkout', [transactionController::class, 'checkout']);
