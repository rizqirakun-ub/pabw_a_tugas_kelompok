<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\produkController;
use App\Http\Controllers\kategoriController;
use App\Http\Controllers\transactionController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

// product
Route::get('/list-all-product', [produkController::class, 'index']);
Route::get('/product-new', [produkController::class, 'create']);
Route::post('/product-save', [produkController::class, 'store']);
Route::post('/product-update/{id}', [produkController::class, 'update']);
Route::get('/product-edit/{id}', [produkController::class, 'edit']);
Route::get('/product-delete/{id}', [produkController::class, 'destroy']);
Route::get('/list-product/{id}', [kategoriController::class, 'show']);

// category
Route::get('/list-category', [kategoriController::class, 'index']);
Route::get('/category-new', [kategoriController::class, 'create']);
Route::post('/category-save', [kategoriController::class, 'store']);
Route::get('/category-edit/{id}', [kategoriController::class, 'edit']);
Route::post('/category-update/{id}', [kategoriController::class, 'update']);
Route::get('/category-delete/{id}', [kategoriController::class, 'destroy']);

// transaction
Route::get('/list-transaction', [transactionController::class, 'index']);
Route::get('/transaction-edit/{id}', [transactionController::class, 'edit']);
Route::post('/transaction-update/{id}', [transactionController::class, 'update']);
Route::get('/transaction-delete/{id}', [transactionController::class, 'destroy']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

Route::get('/vue', function () {
    return view('layouts.vue_app');
});
Route::get('/vue/{any}', function () {
    return view('layouts.vue_app');
})->where('any', '.*');
